**INSTALLATION**

- cd "path_to_project"
- composer install
- mkdir -p config/jwt
- sudo openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
- sudo openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
- Create a MySQL DB :  
  CREATE DATABASE dbname;  
  GRANT ALL PRIVILEGES ON dbname. * TO 'username'@'localhost';  
  FLUSH PRIVILEGES;
- Copy .env & rename it .env.local  
  Set DATABASE_URL & JWT_PASSPHRASE
- bin/console doctrine:schema:create
- bin/console cache:clear
- php -S localhost:8000 -t public