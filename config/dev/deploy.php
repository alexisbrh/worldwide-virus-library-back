<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('debian@51.38.37.43')
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/var/www/wvl-back/html')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@gitlab.com:alexisbrh/worldwide-virus-library-back.git')
            // the repository branch to deploy
            ->repositoryBranch('develop')
            ->composerInstallFlags('--prefer-dist --no-interaction')
            ->sharedFilesAndDirs([
                '.env',
                '.env.dev',
                'config/jwt'
            ])
        ;
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    public function beforePreparing()
    {
//        $this->runRemote(sprintf('cp {{ deploy_dir }}/shared/.env {{ project_dir }} 2>/dev/null'));
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
         $this->runRemote('{{ console_bin }} doctrine:schema:update --force --env=dev');
        // $this->runLocal('say "The deployment has finished."');
    }
};
