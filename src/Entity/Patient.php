<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dead = false;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="patients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity=Virus::class, inversedBy="patients")
     */
    private $virus;

    public function __construct()
    {
        $this->virus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDead(): ?bool
    {
        return $this->dead;
    }

    public function setDead(bool $dead): self
    {
        $this->dead = $dead;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Virus[]
     */
    public function getVirus(): Collection
    {
        return $this->virus;
    }

    public function addViru(Virus $viru): self
    {
        if (!$this->virus->contains($viru)) {
            $this->virus[] = $viru;
        }

        return $this;
    }

    public function removeViru(Virus $viru): self
    {
        if ($this->virus->contains($viru)) {
            $this->virus->removeElement($viru);
        }

        return $this;
    }
}
